## [1.8.3](https://bitbucket.org/superkoders/sk-tools/compare/v1.8.2...v1.8.3) (2024-10-18)


### Features

* set cookie secure as default ([967a76b](https://bitbucket.org/superkoders/sk-tools/commits/967a76bf3998fab77a4115b90094ef23b977332b))



## [1.8.2](https://bitbucket.org/superkoders/sk-tools/compare/v1.7.1...v1.8.2) (2024-05-17)


### Features

* cookie better subdomains handling (including .co.uk domains) ([12b5420](https://bitbucket.org/superkoders/sk-tools/commits/12b5420574f344cba9c0ce6f68b585f0d39e34d3))
* cookie support for multi subdomains ([6d4fff5](https://bitbucket.org/superkoders/sk-tools/commits/6d4fff587953fa52ae7fd7542b217ef3c92aa4f5))



## [1.8.1](https://bitbucket.org/superkoders/sk-tools/compare/v1.8.0...v1.8.1) (2024-01-17)



# [1.8.0](https://bitbucket.org/superkoders/sk-tools/compare/v1.7.1...v1.8.0) (2024-01-17)


### Features

* cookie support for multi subdomains ([6d4fff5](https://bitbucket.org/superkoders/sk-tools/commits/6d4fff587953fa52ae7fd7542b217ef3c92aa4f5))



## [1.7.1](https://bitbucket.org/superkoders/sk-tools/compare/v1.7.0...v1.7.1) (2022-01-28)


### Bug Fixes

* add extension ([f929bd5](https://bitbucket.org/superkoders/sk-tools/commits/f929bd51e3e7e7ce9acac1c94b61e10a1e17480d))



# [1.7.0](https://bitbucket.org/superkoders/sk-tools/compare/v1.6.1...v1.7.0) (2022-01-28)


### Features

* add right "module" type for package ([af65be3](https://bitbucket.org/superkoders/sk-tools/commits/af65be37a392637b1b33d2eb7886d3debd73619c))



## [1.6.1](https://bitbucket.org/superkoders/sk-tools/compare/v1.6.0...v1.6.1) (2021-12-21)


### Bug Fixes

* remove domain from cookie ([5063a5d](https://bitbucket.org/superkoders/sk-tools/commits/5063a5d6896eec71dfe0e95fb359dcb9293bc3b2))



# [1.6.0](https://bitbucket.org/superkoders/sk-tools/compare/v1.4.0...v1.6.0) (2021-12-21)


### Bug Fixes

* add domain to cookie creation ([401a065](https://bitbucket.org/superkoders/sk-tools/commits/401a06523258ec4e14b4189df59c756624585530))
* removeCookie not remove cookie without path ([3ec0dcf](https://bitbucket.org/superkoders/sk-tools/commits/3ec0dcf69d96334c204d68c44edcc0919e487569))
* send memoized state to selector ([c6d3b93](https://bitbucket.org/superkoders/sk-tools/commits/c6d3b9353e1dc3357ce0f58f23f197d9df4dbd45))


### Features

* add dom loader ([bf6e2d4](https://bitbucket.org/superkoders/sk-tools/commits/bf6e2d4678842876396e943f8e8dde1a958de300))
* add getCookie tool ([ed3719c](https://bitbucket.org/superkoders/sk-tools/commits/ed3719cd536585162827e3d1ff1f51bfcd557799))
* add image loader ([d43753c](https://bitbucket.org/superkoders/sk-tools/commits/d43753c95b2ab23ce2337075d5f34d2a0ac83512))
* add mapy.cz loader ([f4c5b23](https://bitbucket.org/superkoders/sk-tools/commits/f4c5b23633ffc05e497d42666510a6b44a038364))
* add once invoked function ([1123f33](https://bitbucket.org/superkoders/sk-tools/commits/1123f333af51b0bbd8e6a6b60938a32e12709311))
* add or/and functional operators ([62b78b7](https://bitbucket.org/superkoders/sk-tools/commits/62b78b76069a59f4735b7b2c354d522c9e4ceb27))
* add replace method ([45b955b](https://bitbucket.org/superkoders/sk-tools/commits/45b955b66ee22d6c2d9d92155816e86e760dfbd7))
* add test for pasive events ([1f7f22b](https://bitbucket.org/superkoders/sk-tools/commits/1f7f22b8a5f55d59edb76ffe54d3fa9aeed6ba33))
* add tool for HTML parser ([e82283c](https://bitbucket.org/superkoders/sk-tools/commits/e82283ce9513836549365eadecb457e750e5d722))
* add tool for objectFit calculation ([0107bb1](https://bitbucket.org/superkoders/sk-tools/commits/0107bb1c8d175bc91852434b08fabaf3f18efe57))
* add tool for promised delay ([11f4607](https://bitbucket.org/superkoders/sk-tools/commits/11f46079e9e79eff4eb9eeefecd5e3dadc2ca47c))



# [1.5.0](https://bitbucket.org/superkoders/sk-tools/compare/v1.4.0...v1.5.0) (2021-11-01)


### Bug Fixes

* send memoized state to selector ([c6d3b93](https://bitbucket.org/superkoders/sk-tools/commits/c6d3b9353e1dc3357ce0f58f23f197d9df4dbd45))


### Features

* add dom loader ([bf6e2d4](https://bitbucket.org/superkoders/sk-tools/commits/bf6e2d4678842876396e943f8e8dde1a958de300))
* add image loader ([d43753c](https://bitbucket.org/superkoders/sk-tools/commits/d43753c95b2ab23ce2337075d5f34d2a0ac83512))
* add mapy.cz loader ([f4c5b23](https://bitbucket.org/superkoders/sk-tools/commits/f4c5b23633ffc05e497d42666510a6b44a038364))
* add once invoked function ([1123f33](https://bitbucket.org/superkoders/sk-tools/commits/1123f333af51b0bbd8e6a6b60938a32e12709311))
* add or/and functional operators ([62b78b7](https://bitbucket.org/superkoders/sk-tools/commits/62b78b76069a59f4735b7b2c354d522c9e4ceb27))
* add replace method ([45b955b](https://bitbucket.org/superkoders/sk-tools/commits/45b955b66ee22d6c2d9d92155816e86e760dfbd7))
* add test for pasive events ([1f7f22b](https://bitbucket.org/superkoders/sk-tools/commits/1f7f22b8a5f55d59edb76ffe54d3fa9aeed6ba33))
* add tool for HTML parser ([e82283c](https://bitbucket.org/superkoders/sk-tools/commits/e82283ce9513836549365eadecb457e750e5d722))
* add tool for objectFit calculation ([0107bb1](https://bitbucket.org/superkoders/sk-tools/commits/0107bb1c8d175bc91852434b08fabaf3f18efe57))
* add tool for promised delay ([11f4607](https://bitbucket.org/superkoders/sk-tools/commits/11f46079e9e79eff4eb9eeefecd5e3dadc2ca47c))



# [1.4.0](https://bitbucket.org/superkoders/sk-tools/compare/v1.3.0...v1.4.0) (2021-03-03)


### Features

* load Vimeo API helper ([a8f1417](https://bitbucket.org/superkoders/sk-tools/commits/a8f1417c574611659c4cb378a61083b895ef7f27))



# [1.3.0](https://bitbucket.org/superkoders/sk-tools/compare/v1.2.0...v1.3.0) (2020-01-08)


### Features

* add "is" method to match if element is selector range ([a105403](https://bitbucket.org/superkoders/sk-tools/commits/a1054030a4f47614e25d908f174c7e8e2a3ed4c2))
* add better option for after paint callback ([f47a881](https://bitbucket.org/superkoders/sk-tools/commits/f47a881a253cadfeb0c9548cb919b80c616e75b4))
* add isMac for browser testing ([e85e953](https://bitbucket.org/superkoders/sk-tools/commits/e85e953cae216fcc424345696d31e192147018c7))



# [1.2.0](https://bitbucket.org/superkoders/sk-tools/compare/v1.1.2...v1.2.0) (2020-01-03)


### Bug Fixes

* create unique callback to fix collisions ([6db2cae](https://bitbucket.org/superkoders/sk-tools/commits/6db2cae41b78cfded9b1eef1946c79d61d493482))
* generate changelog without global npm dependency ([2cfb1c2](https://bitbucket.org/superkoders/sk-tools/commits/2cfb1c210cc852fb1dd7659296d9fddfc4da2306))
* syntax error ([d7e3ea5](https://bitbucket.org/superkoders/sk-tools/commits/d7e3ea573ab62ca141512e157860e4e99dc51f4a))


### Features

* add browser test for object-fit ([30df352](https://bitbucket.org/superkoders/sk-tools/commits/30df352f1fcf10935e8ed7c2b49dc847f25ba217))



## [1.1.2](https://bitbucket.org/superkoders/sk-tools/compare/v1.1.1...v1.1.2) (2019-09-13)



## [1.1.1](https://bitbucket.org/superkoders/sk-tools/compare/v1.1.0...v1.1.1) (2019-09-13)


### Bug Fixes

* import query function for delagate helper ([ddba503](https://bitbucket.org/superkoders/sk-tools/commits/ddba503))



# [1.1.0](https://bitbucket.org/superkoders/sk-tools/compare/v1.0.2...v1.1.0) (2019-08-19)


### Bug Fixes

* path to array clone ([67f2eb9](https://bitbucket.org/superkoders/sk-tools/commits/67f2eb9))
* readme ([85b1c21](https://bitbucket.org/superkoders/sk-tools/commits/85b1c21))
* spacing format ([f750324](https://bitbucket.org/superkoders/sk-tools/commits/f750324))


### Features

* add new traversing functions: prev, next, prevAll, nextAll, closest ([7629a60](https://bitbucket.org/superkoders/sk-tools/commits/7629a60))
* new testing function isWindow ([d11fe0f](https://bitbucket.org/superkoders/sk-tools/commits/d11fe0f))



## [1.0.2](https://bitbucket.org/superkoders/sk-tools/compare/v1.0.1...v1.0.2) (2019-07-09)


### Bug Fixes

* remove old hardcoded google api key ([b87342a](https://bitbucket.org/superkoders/sk-tools/commits/b87342a))



## [1.0.1](https://bitbucket.org/superkoders/sk-tools/compare/v1.0.0...v1.0.1) (2019-07-09)



# 1.0.0 (2019-07-09)


### Features

* add library to npm ([39d4ae6](https://bitbucket.org/superkoders/sk-tools/commits/39d4ae6))



