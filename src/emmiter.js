import { inArray, arrayOrEmpty } from "./array.js";
import { is, isFunction } from "./logic.js";

export const create = (target) => {
	let subscribers = {};

	const withEmmiter = {
		...target,
		on(type, fn) {
			let arr = arrayOrEmpty(subscribers[type]);

			if (!inArray(fn, arr) && isFunction(fn)) {
				subscribers[type] = [...arr, fn];
			}

			return withEmmiter;
		},
		one(type, fn) {
			fn._event_once = true;
			return withEmmiter.on(type, fn);
		},
		off(type, fn) {
			if (type == null && fn == null) {
				Object.keys(subscribers).forEach((type) => withEmmiter.off(type));
			}

			if (!type || !subscribers[type]) return withEmmiter;

			subscribers[type] = subscribers[type].filter((fnI) => {
				// console.log(is(fn, fnI))
				if (!fn || is(fn, fnI)) {
					delete fnI._event_once;
					return false;
				}
				return true;
			});

			if (!subscribers[type].length) delete subscribers[type];

			return withEmmiter;
		},
		trigger(type, data, context = null) {
			arrayOrEmpty(subscribers[type])
				.filter((fn) => {
					fn.call(context, { type }, data);
					return fn._event_once;
				})
				.forEach(withEmmiter.off.bind(withEmmiter, type));

			return withEmmiter;
		},
		log() {
			console.log(subscribers);
			return withEmmiter;
		},
	};

	return withEmmiter;
};

const global = create({});

export const on = global.on;
export const one = global.one;
export const off = global.off;
export const trigger = global.trigger;
export const log = global.log;
