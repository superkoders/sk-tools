export const ARROW_LEFT = 37;
export const ARROW_UP = 38;
export const ARROW_RIGHT = 39;
export const ARROW_DOWN = 40;
export const ENTER = 13;
export const SPACE = 32;
export const ESCAPE = 27;

export const keys = (...args) => args.join(',');

export const observer = (target, props, type = 'keydown') => {
	const onKeyDown = (event) => {
		Object.keys(props)
			.filter((key) => {
				return key.split(',').includes(`${event.keyCode}`);
			})
			.forEach((key) => {
				props[key](event);
			});
	};

	target.addEventListener(type, onKeyDown);

	return () => {
		target.removeEventListener(type, onKeyDown);
	};
};
