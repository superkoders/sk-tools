import { curry } from "./function.js";

export const first = (arr) => {
	return arr[0] || null;
};

export const last = (arr) => {
	return arr[arr.length - 1] || null;
};

export const length = (arr) => {
	return arr.length;
};

export const clone = (arr) => {
	return [].slice.call(arr);
};

export const map = curry((fn, arr) => {
	return arr.map(fn);
});

export const flatten = (arr) => {
	return arr.reduce((acc, cur) => acc.concat(cur), []);
};

export const join = curry((delimiter, arr) => {
	return arr.join(delimiter);
});

export const inArray = (value, arr) => {
	return arr.indexOf(value) > -1;
};

export const arrayOrEmpty = (arr) => {
	return arr || [];
};

export const groupBy = curry((prop, arr) => {
	return arr.reduce((acc, cur) => {
		return {
			...acc,
			[cur[prop]]: cur,
		};
	}, {});
});

export const emptyBySize = (size) => {
	return Array.apply(null, Array(size));
};
