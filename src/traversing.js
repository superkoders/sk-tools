import { clone } from "./array.js";

export const parent = (node) => {
	return node.parentNode;
};

export const children = (node) => {
	return clone(node.children);
};

export const siblings = (node) => {
	children(parent(node)).filter((child) => {
		return child !== node;
	});
};

export const prev = (node) => {
	return node.previousElementSibling;
};

export const next = (node) => {
	return node.nextElementSibling;
};

const dir = (node, getNext) => {
	let matched = [];
	let cur = getNext(node);
	while (cur != null) {
		matched = matched.concat(cur);
		cur = getNext(cur);
	}
	return matched;
};

export const prevAll = (node) => {
	return dir(node, (node) => prev(node));
};

export const nextAll = (node) => {
	return dir(node, (node) => next(node));
};

export const closest = (node, selector) => {
	return node.closest(selector);
};
