import { on, off } from "./event.js";

export const transition = (element, styles) => {
	return new Promise((resolve) => {
		const onEnd = (event) => {
			if (event.target !== element) return;
			if (styles[event.propertyName] == null) return;
			off(element, "transitionend", onEnd);
			resolve();
		};
		on(element, "transitionend", onEnd);
		Object.keys(styles).forEach((key) => {
			element.style[key] = styles[key];
		});
	});
};
