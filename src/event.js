import { query } from "./selector.js";
import { once } from "./function.js";

export const delegate = (selector, callback) => (event) => {
	const node = query(selector, event.currentTarget).find((node) =>
		node.contains(event.target)
	);

	if (node != null) {
		callback(event, node);
	}

	return event;
};

export const on = (node, type, handler) => {
	node.addEventListener(type, handler, false);
};

export const off = (node, type, handler) => {
	node.removeEventListener(type, handler);
};

// Test via a getter in the options object to see if the passive property is accessed
export const hasPassiveEvent = once(() => {
	let supportsPassive = false;
	try {
		var opts = Object.defineProperty({}, "passive", {
			get: function () {
				supportsPassive = true;
			},
		});
		window.addEventListener("testPassive", null, opts);
		window.removeEventListener("testPassive", null, opts);
	} catch (e) {}

	return supportsPassive;
});
