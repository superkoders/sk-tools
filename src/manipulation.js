export const remove = (node) => {
	node.parentNode.removeChild(node);
};

export const empty = (node) => {
	node.textContent = '';
};

export const wrap = (node, wrapper) => {
	node.parentNode.insertBefore(wrapper, node);
	wrapper.appendChild(node);
};

export const unwrap = (node) => {
	const wrapper = node.parentNode;
	const parent = wrapper.parentNode;
	parent.insertBefore(node, wrapper);
	parent.removeChild(wrapper);
};

export const replace = (node, content) => {
	node.parentNode.replaceChild(content, node);
};

export const append = (node, content) => {
	node.appendChild(content);
};

export const prepend = (node, content) => {
	node.insertBefore(content, node.firstChild);
};

export const before = (node, content) => {
	node.parentNode.insertBefore(content, node);
};

export const after = (node, content) => {
	node.parentNode.insertBefore(content, node.nextSibling);
};
