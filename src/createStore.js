export default (initialState) => {
	let currentState = initialState;
	let subscribers = [];
	let actions = [];
	let timer = null;

	const dispatch = (action, immediately) => {
		actions.push(action);

		if (immediately) {
			cancelAnimationFrame(timer);
			update();
			return;
		}
		if (timer == null) {
			timer = window.requestAnimationFrame(update);
		}
	};

	const unsubscribe = (callback) => {
		subscribers = subscribers.filter((subscriber) => {
			return subscriber !== callback;
		});
	};

	const update = () => {
		if (!actions.length) return;
		const prevActions = [...actions];
		actions = [];
		const state = prevActions.reduce((accState, action) => action(accState, dispatch), currentState);

		timer = null;

		if (state === currentState) return;
		subscribers.forEach((subscriber) => subscriber(state, currentState));
		currentState = state;
	};

	return {
		getState() {
			return currentState;
		},
		dispatch(...args) {
			dispatch(...args);
			return this;
		},
		subscribe(callback, callNow = false) {
			subscribers.push(callback);
			callNow && callback(currentState, {});
			return unsubscribe.bind(null, callback);
		},
	};
};
