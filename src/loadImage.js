export const loadImage = (url) => {
	return new Promise((resolve, reject) => {
		const s = document.createElement('img');
		s.onload = () => resolve(s);
		s.onerror = reject;
		s.src = url;
	});
};