import loadScript from "./loadScript.js";

let api = null;

export default () => {
	if (api) return api;

	api = new Promise((resolve, reject) => {
		loadScript("https://player.vimeo.com/api/player.js")
			.then(() => resolve(window.Vimeo))
			.catch(reject);
	});

	return api;
};
