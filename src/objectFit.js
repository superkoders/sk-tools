export const objectFit = (element, target, props = {}) => {
	const { width, height } = element;
	const { width: currentWidth, height: currentHeight } = target;
	const { type = "cover", alignX = 0.5, alignY = 0.5 } = props;
	const mathFn = type === "cover" ? "max" : "min";
	const coef = Math[mathFn](currentWidth / width, currentHeight / height);

	return {
		width: width * coef,
		height: height * coef,
		offsetY: Math.round((currentHeight - height * coef) * alignY),
		offsetX: Math.round((currentWidth - width * coef) * alignX),
	};
};
