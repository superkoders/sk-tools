import loadScript from "./loadScript.js";

let api = null;

export default (key) => {
	if (api) return api;

	api = new Promise((resolve, reject) => {
		const callback = `GMapsCallback${new Date().getTime()}`;
		window[callback] = () => {
			resolve(window.google.maps);
			delete window[callback];
		};

		loadScript(
			`https://maps.googleapis.com/maps/api/js?key=${key}&callback=${callback}`
		).catch(reject);
	});

	return api;
};
