export const is = (a, b) => {
	return a === b ?
	(a !== 0 || 1 / a !== 1 / b) :
	(a !== a && b !== b);
};

export const isFunction = (fn) => {
	return is(typeof fn, 'function');
};

export const isObject = (obj) => {
	return obj === Object(obj);
};

export const isNil = (x) => {
	return x == null;
};

export const isWindow = (node) => {
	return node != null && node === node.window;
}

const or = (...predicates) => (...args) => {
	return predicates.some((predicate) => predicate(...args));
};

const and = (...predicates) => (...args) => {
	return predicates.every((predicate) => predicate(...args));
};
