import { curry } from "./function.js";
import { isObject } from "./logic.js";

// pickAs(['clientX:  x', 'clientY: y'], event);
const pickAsReg = /:\s*/;
export const pickAs = (props, obj) => {
	return props.reduce((acc, val) => {
		val = val.split(pickAsReg);
		if (obj[val[0]]) {
			acc[val[1] || val[0]] = obj[val[0]];
		}
		return acc;
	}, {});
};

export const prop = curry((prop, obj) => obj[prop]);

export const propPath = curry((path, obj) => {
	const [first, ...rest] = path.split(".");
	const val = prop(first, obj);

	return rest.length === 0
		? val
		: !isObject(val) || val == null
		? null
		: propPath(rest.join("."), val);
});
