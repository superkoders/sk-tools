import loadScript from "./loadScript.js";

let api = null;

export default () => {
	if (api) return api;

	api = new Promise((resolve, reject) => {
		window.onYouTubeIframeAPIReady = () => {
			resolve(window.YT);
			delete window.onYouTubeIframeAPIReady;
		};

		loadScript("//www.youtube.com/player_api").catch(reject);
	});

	return api;
};
