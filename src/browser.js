const ua = navigator.userAgent.toLowerCase();

export const isIOS = /ipad|iphone|ipod/.test(ua) && !window.MSStream;
export const isAndroid = ua.includes('android');
export const isFF = 'MozAppearance' in document.documentElement.style;
export const isLteIE10 = document.all && window.XMLHttpRequest;
export const isIE11 = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
export const isIE = isLteIE10 || isIE11;
export const isMac = navigator.platform.indexOf('Mac') > -1;

export const supportsCover = 'CSS' in window && typeof CSS.supports === 'function' && CSS.supports('object-fit: cover');
