import { once } from "./function.js";

export const loadDom = once(() => {
	return new Promise((resolve) => {
		if (document.readyState == "loading") {
			document.addEventListener("DOMContentLoaded", resolve);
		} else {
			resolve();
		}
	});
});
