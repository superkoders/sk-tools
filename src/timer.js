const win = window;

export const onTimeout = (time, callback) => {
	return win.setTimeout(() => callback(), time);
};

export const offTimeout = (id) => {
	return win.clearTimeout(id);
};

export const onInterval = (time, callback) => {
	return win.setInterval(() => callback(), time);
};

export const offInterval = (id) => {
	return win.clearInterval(id);
};

export const onFrame = (callback) => {
	return win.requestAnimationFrame(callback);
};

export const offFrame = (id) => {
	return win.cancelAnimationFrame(id);
};

export const afterQueue = (callback) => {
	return onTimeout(0, callback);
};

export const afterPaint = (callback) => {
	return onFrame(afterQueue.bind(null, callback));
};

export const afterPaintRaf = (callback) => {
	return onFrame(onFrame.bind(null, callback));
};

export const delay = (time) => {
	return new Promise((resolve) => onTimeout(time, resolve));
};
