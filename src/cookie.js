const getCookieString = (name, value, expires, domain, path, secure) => {
	return `${name}=${value}; ${expires}; domain=${domain}; path=${path}${secure ? '; secure' : ''}`;
};

export const addCookie = (name, value, expiration, path = "/", domain, secure = true) => {
	const date = new Date();
	date.setTime(date.getTime() + expiration * 1000 * 60 * 60 * 24);
	const expires = `expires=${date.toGMTString()}`;
	const host = location.host;
	const domainParts = host.split(".");

	if (domain || domainParts.length === 1) {
		document.cookie = getCookieString(
			name,
			value,
			expires,
			domain || host,
			path,
			secure
		);
	} else {
		domainParts.shift();

		document.cookie = getCookieString(
			name,
			value,
			expires,
			`.${domainParts.join(".")}`,
			path,
			secure
		);

		// Check if cookie was saved. If not, we were probably trying to save cookie to top-level domain
		if (getCookie(name) === null || getCookie(name) !== value) {
			document.cookie = getCookieString(name, value, expires, `.${host}`, path, secure);
		}
	}
};

export const removeCookie = (name, path = "/", domain) => {
	addCookie(name, "", -1, path, domain);
};

export const hasCookie = (name) => {
	return document.cookie.indexOf(`${name}=`) > -1;
};

export const getCookie = (name) => {
	var match = document.cookie.match(new RegExp(`(^| )${name}=([^;]+)`));
	if (match && match[2]) return match[2];
	return null;
};
