export const curry = (f, ...args) => {
	if (args.length >= f.length) return f(...args);
	return (...next) => curry(f.bind(f, ...args), ...next);
};

export const pipe = (...fns) => x => fns.reduce((v, f) => f(v), x);

export const tap = curry((fn, arg) => {
	fn(arg);
	return arg;
});

export const invoke = curry((fnName, args, obj) => {
	return obj[fnName].apply(obj, args);
});


export const debounce = curry((wait, func) => {
	let timeout;
	return (...args) => {
		var later = () => {
			timeout = null;
			func(...args);
		};
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
	};
});

export const once = (fn) => {
	let ret;
	let isCalled = false;
	return (...args) => {
		if (isCalled === true) return ret;
		isCalled = true;
		ret = fn(...args);
		return ret;
	}
}
