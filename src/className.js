export const hasClass = (node, className) => {
	if (node.classList) return node.classList.contains(className);

	return (` ${node.className.trim()} `).indexOf(` ${className} `) > -1;
};

export const addClass = (node, className) => {
	if (node.classList) {
		node.classList.add(className);
	} else if (!hasClass(node, className)) {
		node.className = (`${node.className.trim()} ${className}`).trim();
	}
	return node;
};

export const removeClass = (node, className) => {
	if (node.classList) {
		node.classList.remove(className);
	} else {
		node.className = (` ${node.className.trim()} `).replace(` ${className} `, '').trim();
	}
	return node;
};


export const toggleClass = (node, className) => {
	if (node.classList) {
		node.classList.toggle(className);
		return node;
	}
	
	return hasClass(node, className) ?
		removeClass(node, className) :
		addClass(node, className);
};

export const boolClass = (node, className, bool) => {
	return (bool === true) ?
		addClass(node, className) :
		removeClass(node, className);
};
