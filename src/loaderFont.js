import FontFaceObserver from "fontfaceobserver";
import { afterPaint } from "./timer.js";
import { hasClass, addClass } from "./className.js";

const html = document.documentElement;

export default (list, { className = "fonts-loaded" }) => {
	return new Promise((resolve) => {
		if (hasClass(html, className)) return resolve();

		Promise.all(
			list.map(([name, props]) => {
				return new FontFaceObserver(name, props).load();
			})
		).then(() => {
			addClass(html, className);
			afterPaint(resolve);
		});
	});
};
