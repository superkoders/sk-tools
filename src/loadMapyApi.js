import loadScript from "./loadScript.js";

let api = null;

export const loadMapyApi = () => {
	if (api != null) return promise;

	api = new Promise((resolve) => {
		loadScript("//api.mapy.cz/loader.js").then(() => {
			const { Loader } = window;
			Loader.async = true;
			Loader.load(null, null, () => {
				resolve(window.SMap);
			});
		});
	});

	return api;
};
