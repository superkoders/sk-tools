export const min = (a, b) => {
	return b < a ? b : a;
};

export const max = (a, b) => {
	return b > a ? b : a;
};

export const clamp = (minVal, maxVal, val) => max(
	min(minVal, maxVal),
	min(
		max(minVal, maxVal),
		val,
	),
);

export const round = (number) => {
	return Math.round(number);
};

export const getDistance = (minVal, val) => (val - minVal);

export const getProgress = (minVal, maxVal, val) => getDistance(minVal, val) / getDistance(minVal, maxVal);

export const getPercentDistance = (...args) => getProgress(...args) * 100;

export const diffPoints = (from, to) => {
	const x = to.x - from.x;
	const y = to.y - from.y;

	return {
		x,
		y,
		dist: Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)),
	};
};

export const toDegrees = (radians) => {
	return radians * 180 / Math.PI;
};

export const toRadians = (degrees) => {
	return degrees * Math.PI / 180;
};

export const getPointsAngle = (from, to) => {
	const {x, y} = diffPoints(from, to);
	return Math.atan2(y, x);
}

export const getIntersectionPoint = (point, radius, angle) => {
	return {
		x: point.x + radius * Math.cos(angle),
		y: point.y + radius * Math.sin(angle)
	}
}