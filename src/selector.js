import { clone } from "./array.js";

export const byID = (selector, context = document) => {
	return [context.getElementById(selector)];
};

export const byName = (selector, context = document) => {
	return clone(context.getElementsByName(selector));
};

export const query = (selector, context = document) => {
	return clone(context.querySelectorAll(selector));
};

export const is = (node, selector) => {
	return node.matches(selector);
};
