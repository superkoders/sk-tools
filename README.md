# SUPERKODERS Tools

Toolkit of helper functions

### Usage
```
import {addClass} from '@superkoders/sk-tools/src/className'
```

Tools which you can import separately. If your build process can tree shaking you get the smallest file.
Documentation will be created. For now read from code
[https://bitbucket.org/superkoders/sk-tools/src/master/src/](https://bitbucket.org/superkoders/sk-tools/src/master/src/)
